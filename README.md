# Short exercise on jets

## Exercise 1

Execution:
```shell
make ex01
```

Exercise:
 - Implement the `cluster()` function according to a sequence clustering algorithm
 - Play with parameters *R* and *p*

## Exercise 2

Execution:
```shell
make ex02
```

Exercise:
 - Use FastJet to cluster the same particles
 - Compare the results
